export function isPropertyValue(value) {
  // Cf https://schema.org/PropertyValue
  return value && "name" in value && "value" in value;
}

export function toServiceChannel(properties, serviceInfoMap, themeLabelMap) {
  const {
    siret,
    nom,
    adresse,
    code_postal,
    commune,
    e_mail,
    telephone,
    site_web,
    horaires,
    services,
  } = properties;

  const providedServices = services
    ? services.split("|").map((serviceId) => {
        const serviceInfo = serviceInfoMap[serviceId];
        const themeId = serviceInfo.theme_code;
        return {
          category: {
            identifier: themeId,
            name: themeLabelMap[themeId],
          },
          identifier: serviceId,
          name: serviceInfo.label,
        };
      })
    : null;

  return {
    // Cf https://schema.org/ServiceChannel
    name: nom,
    identifier: siret,
    serviceLocation: {
      // Cf https://schema.org/CivicStructure
      address: {
        // Cf https://schema.org/PostalAddress
        streetAddress: adresse,
        postalCode: code_postal,
        addressLocality: commune,
        email: e_mail,
        telephone,
      },
      // No need for geo coordinates because it's already in GeoJSON.
      // latitude,
      // longitude,
      openingHours: horaires,
      url: site_web,
    },
    providesService: providedServices,
  };
}
