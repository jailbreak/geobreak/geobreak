import lunr from "lunr";
import lunrFr from "lunr-languages/lunr.fr";
import lunrStemmerSupport from "lunr-languages/lunr.stemmer.support";

lunrStemmerSupport(lunr);
lunrFr(lunr);

export function indexPlaces(docs) {
  return lunr(function () {
    this.use(lunr.fr);
    this.ref("id_source");
    this.field("nom");

    docs.forEach(doc => {
      this.add(doc);
    });
  });
}
