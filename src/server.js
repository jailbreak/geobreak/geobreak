import * as sapper from '@sapper/server'

import compression from 'compression'
import config from "./config.js"
import polka from 'polka'
import sirv from 'sirv'

const { PORT, NODE_ENV } = process.env
const dev = NODE_ENV === 'development'

polka() // You can also use Express
  .use(
    config.basePath,
    compression({ threshold: 0 }),
    sirv('static', { dev }),
    sapper.middleware({
      session: () => ({
        mapboxAccessToken: config.mapboxAccessToken
      })
    })

  )
  .listen(PORT, err => {
    if (err) console.log('error', err)
  })
