import { writable } from 'svelte/store'

export function createSelection() {
  const { subscribe, update } = writable()
  let _map
  return {
    subscribe,
    set: (newValue) => {
      update(currentValue => {
        if (currentValue) {
          _map.setFeatureState(currentValue.feature, { selected: false });

        }
        if (newValue) {
          _map.setFeatureState(newValue.feature, { selected: true });
        }
        return newValue
      })
    },
    setMap: (map) => {
      _map = map
    },
  }
}

export const selection = createSelection()
