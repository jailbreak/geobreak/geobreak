import { cleanEnv, str } from "envalid"

const env = cleanEnv(process.env, {
  BASE_PATH: str({ default: "/" }),
  MAPBOX_ACCESS_TOKEN: str(),
})

export default {
  basePath: env.BASE_PATH,
  mapboxAccessToken: env.MAPBOX_ACCESS_TOKEN,
}
